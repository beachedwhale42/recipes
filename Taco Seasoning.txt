Taco Seasoning

Ingredients:
1 TBS chili powder
1/4 teaspoon garlic powder
1/4 teaspoon crushed red pepper flakes
1/4 teaspoon dried oregano
1/2 teaspoon paprika
1 1/2 teaspoon ground cumin
1 teaspoon black pepper
1/2 teaspoon ancho chile pepper

Directions:
Mix, smell, and be happy