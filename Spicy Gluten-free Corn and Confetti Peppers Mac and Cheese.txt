Spicy Gluten-free Corn and Confetti Peppers Mac and Cheese By Neelima Gogumalla, Windham

Ingredients:
36 oz. mini corn pasta in any shape
1 large Vidalia onion, diced
2 cloves fresh garlic, minced
1 large jalapeno, minced
3 teaspoons ground cumin and coriander
pepper
2 teaspoons crushed red pepper
2 teaspoons chili powder
2 large green pepper, diced
1 large red pepper, diced
1 large yellow pepper, diced
1 large orange pepper, diced
2 cups corn
18 oz. Pepper Jack cheese, grated
18 oz. sharp cheddar cheese, grated
2 tablespoons finely ground cornmeal
2 teaspoons garlic powder
1 teaspoons prepared grainy mustard
3 cups milk, warmed
1 cup half and half
1 scant cup chicken stock or water
2 cups small curd cottage cheese or queso
fresco
2 eggs beaten
4 tablespoons olive oil
2 tablespoons butter
Sea salt and freshly ground pepper
1 cup coarsely crushed corn chips

Directions:
Heat oven to 375 degrees. Heat 3 tablespoons olive oil in a saute pan.

Add the spice powders and toast 30 seconds until fragrant. Add onion, garlic, peppers, salt and saute until the vegetable are soft. Drain any excess oil.

Add 1 cup of corn and set aside. Prepare the pasta according to package directions, but remove from the flame one minute early, drain and rinse with warm water and set aside.

Heat 1 tablespoon olive oil and 1 tablespoon butter in a saucepan. Add the cornmeal and garlic powder, and saute until nutty and fragrant. Add the warmed half and half and chicken stock, and bring to simmer until slightly thick.

Take off the flame and cool slightly. Add the beaten eggs, cottage cheese and mustard powder, and combine the sauce.

Mix in the pepper jack cheese and half the cheddar cheese. Add the pasta and the sauteed vegetables, salt and pepper to taste, and pour into a greased baking dish. Pour on the sauce and mix gently.

Melt 1 tablespoon butter and combine with the crushed chips. Spread the remaining cup of corn and 6 oz. cheddar cheese over the top of the pasta, and sprinkle the chips over that and press lightly. 

Cover with foil and bake for 25 minutes, and remove foil and bake for another 10 minutes, until chips are slightly brown and toasty.

Scoop and serve with a salad of sliced avocados, sliced yellow and red tomatoes and shredded lettuce.
 
