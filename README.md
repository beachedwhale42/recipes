# README #

This repository is not source code, but rather recipes stored in plaintext.  They can be read by just about any editor in the world, but were designed to be behind a self-written front end called [LEATHER](https://bitbucket.org/beachedwhale42/leather) (Library of Electronic and Active Thoughts Having Extraordinary Recipes).

### What is this repository for? ###

* My family recipes
* Delicious recipes
* More recipes
* Your recipes

### How do I get set up? ###

* Clone the repo like a normal developer
* Download the repo like a normal person (see Downloads on the left)

### Contribution guidelines ###

* Add your recipes at will
* More food, more better
* Please don't modify other peoples recipes
* If you do, add it as a new recipe, noting it's your version

### Who do I talk to? ###

* Me