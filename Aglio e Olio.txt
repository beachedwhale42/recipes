Aglio e Olio (originally from ItalianChef.com)

Ingredients:
1lb pasta
1/2 cup EVOO
2 garlic cloves, sliced thin
pinch of crushed red pepper
pinch of salt
pinch of fresh cracked black pepper
flat leaf parsley, chopped for garnish

Directions:
Bring a large pot of liberally salted water to a boil. Add the pasta and cook until al dente and strain the pasta.

Meanwhile, heat the extra-virgin olive oil over medium heat in a saute pan large enough to hold the pasta.  Add the garlic, red pepper flakes, salt, and pepper.  You may need to drop the heat to low to prevent the oil from jumping.  Cook until the garlic just begins to turn golden brown.

Add the pasta to the saute pan and toss until well coated.

Transfer the pasta to warm serving plates, garnish with the chopped parsley and serve with grated parmigiano-reggiano or pecorino-romano cheese on the side.