Chocolate Cream Pie

Ingredients:
1 cup granulated sugar
1/4 cup cornstarch
1/4 tsp salt
2 3/4 cups milk
3 egg yolks
1 TBSP butter or margarine, softened
1 tsp vanilla
3 oz unsweetened chocolate, coarsely choppped
Whipped topping
Chocolate shavings or curls (optional)

Directions:
l. In a medium saucepan, combine sugar, cornstarch and salt and mix to blend. Over medium heat, whisk in milk until mixture is smooth, and, stirring constantly, bring to a boil. Boil for I to 2 minutes, stirring gently until slightly thickened. Remove from heat.

2. In a small bowl, beat egg yolks and slowly whisk in about 1/2 to I cup (125 to 250 ml.) of the hot milk mixture and pour that back into the saucepan. Whisk constantly and return to a boil. Boil for I to 2 minutes more until thick.

3. Remove from heat and whisk in butter and vanilla, then the chocolate, and whisk until mixture is blended and completely smooth.

4. Pour into baked pastry crust and cool for 15 to 20 minutes on a wire rack. Cover the filling with a piece of plastic wrap or foil and chill in refrigerator for 5 hours, or overnight. When ready to serve, spread whipped topping either around the edge of pie, or over the complete pie. Garnish with chocolate shavings, if desired.

VARIATION
Black Forest Pie: Take I can (19 oz/540 mL) of cherry pie filling and divide in half. Before spooning chocolate filling into pie crust, fold in half of the cherry pie filling and then refrigerate as above. Just before serving, top with the remaining half of cherry pie filling. Garnish with whipped topping, if desired.
 
