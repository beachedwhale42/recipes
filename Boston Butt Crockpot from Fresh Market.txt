Boston Butt for the Crockpot from Fresh Market

Ingredients:
3-4 lbs Boneless Pork Shoulder (Boston Butt)
1/2 c water
salt & pepper
8 oz TFM Barbecue Sauce

Directions:
Place Meat in Slow Cooker with Water, Salt and Pepper

Cook on HIGH for 1 HR

Turn to LOW and cook for 5 HRS, or until very tender
Remove Pork Shoulder and discard juices.

Shred Pork using 2 forks and return to Slow Cooker. Remove excess fat as you are shredding the roast.

Lightly Mix Barbecue Sauce into Meat for Flavor
Cook on Low for 1 HR or until Hot.

Serve on Sandwich Buns with extra Barbecue Sauce on the side
 
