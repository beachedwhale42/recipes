Black Truffle Pasta

Ingredients:
400 gr (14oz) fettuccine
Dry Porcini mushrooms
1 small onion
2 garlic cloves
30 gr (2T) butter
Olive oil
White wine
Parmigiano Reggiano cheese
Salt & Pepper
Parsley
Small black truffle

Directions:
Cook the pasta until al dente, meanwhile soak the mushroom in warm water - enough to cover them - for a couple minutes.  A variation of this can be obtained by sautee fresh Porcini mushroom, diced.  Melt the butter in a separate pan, over high heat, add the onion and the garlic cloves (peeled, if you don't mind a strong taste of garlic).  Drain the mushrooms, cut into smaller pieces, then add to the pan.  After a couple minutes add a dash of white wine and the water you have used for the mushrooms.

Let the water dry a bit, then sautee the pasta in the same skillet.  Guarnish with grated cheese, olive oil and sliced black truffle. Serves 4.