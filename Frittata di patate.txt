Frittata di patate (from Il Cucchiaio d'Argento)

Ingredients:
6 eggs
500g potatoes
100g fontina (70g cubed, 30g grated)
1 yellow onion
parsley
EVOO
nutmeg
salt
pepper

Directions:
Peel the potatoes and cut into slices of 5mm thick.  Cook them in boiling salt water for 10 minutes, being careful not to break them.  Drain and let cool.

In a bowl, beat the eggs with the chopped parsley, grated fontina, salt, pepper, and nutmeg.  Add the warm potatoes and mix.

In a 10-11" pan, saute the onion in the oil.  When onion is transparent, add the egg mixture and cubed fontina.  Cover with the lid and cook on medium heat for 15 minutes, moving the pan from time to time to detatch the frittata from the bototm.

Using the cover to help, turn the frittata and slide it back into the pan.  Continue cooking, uncovered, for another 10-15 minutes.

Serve hot or room temperature.