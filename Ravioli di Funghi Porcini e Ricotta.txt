Ravioli di Funghi Porcini e Ricotta

Ingredients:
Ravioli:
Fresh pasta (300g/2.5 cups unbleached AP flour, 3 eggs)
8oz ricotta cheese
1oz bag of dried porcini mushrooms, reconstituted per its instructions, then diced

Garlic cream sauce:
1 brick (8oz) cream cheese
1 stick (8Tbps) butter
1 cup milk
1/2ish cup wine (Orvieto Classico preferred)
1 cup parmesan cheese
A little garlic powder, to taste
A little black pepper, to taste

Makes about 36 ravioli.  Sauce covers about 24.  This is not an exact science.

Directions:
Dice the porcini mushrooms, and mix with the ricotta to desired proportion.  You may not use all mushtooms...this is a "to taste" ratio.  You want the flavor of the mushrooms, but they are strong, and you don't want them overpowering the rest of the dish.

Take a lasagna-sized (or longer) strip of fresh pasta.  Note that it'll be folded on top of itself, so only add filling to half the strip, leaving room between each mound.  You'll need enough room to press down and crimp the top dough with the bottom dough.

Once you eyeball how many ravioli you can make with the strip, add one spoonful of ricotta/porcini mixture per ravioli.

Fold the strip over on itself (so half the strip becomes the top of the ravioli).  Press down around the edges and around each mound, then use a pizza cutter or table knife to cut each piece from the strip.  Using a muddler or a meat tenderizer, (or something that has a "smashing" design) smash/crimp the edges of each ravioli together, keeping the ricotta/mushroom filling inside.

Cook as normal fresh pasta.

The garlic cream sauce is just like an alfredo sauce, but ONLY add a dash of pepper and a little bit of garlic powder.  This keeps the sauce simple and basic, and allows the rest of the dish to provide the flavoring.  Orvieto Classico is the preferred wine to add (after all, this is a very Umbrian dish), but Pinot Grigio would work too.  Add wine to thin sauce out to desired consistency.  For this dish, thinner is usually better.

Drizzle garlic cream sauce over ravioli, to taste.

Pairs best with an Orvieto Classico, a dry white wine, but with a bit of acidity.  Orvieto is a town in the Italian region of Umbria, where this is a common dish.  It is the perfect wine with this dish.  Orvieto Classicos can be hard to find though, so Pinot Grigio would suffice.