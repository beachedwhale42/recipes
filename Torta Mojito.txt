Torta Mojito (from Il Cucchiaio d'Argento)

Ingredients:

For the crust:
300g dried cookies
150g melted butter
(alternatively, use the graham cracker crust from the Cheesecake recipe)

For the filling:
300g ricotta
150ml fresh (or heavy) cream
130g granulated sugar
3 limes
40g rum
8g gelatin (4 sheets, or 1 packet Knox Gelatine powder)
12 mint leaves, finely chopped

For the decoration/topping:
1 lime
mint leaves
cane sugar

Directions:
1. Put the cookies in the mixer and pulverize.  Add the butter and amalgamate.  Use this mix to line a rectangular dish with removable bottom of 35x11cm.  This equates to a round 8.7" diameter pan.  Start first from the edges and then finish with the base by leveling carefully with the back of a spoon.  Put everything in the refrigerator until needed.

2. Soak the gelatin in cold water for the time indicated on the package (approximately 10 minutes).  If using powder, dissolve it in the rum.  In a large bowl, using an electrix mixer, beat the ricotta, the granulated sugar, 12 finely chopped mint leaves, the zest of a lime, and the filtered juice of 3.  Work the whole thing until you get a homogeneous mixture.  In a separate bowl, whip the cream and incorporate it to the cheese-based compound in several stages with movements from bottom to top.

3. Dissolve the squeezed gelatin in a small saucepan together with the rum.  Add everything to the mixture and mix, always gently, with a spatula.

4. Pull the crust out of the refrigerator and pour the mixture into the crust.  Solidify in the fridge for at least 3 hours.  Just before serving, decorate with slices and wedges of lime, fresh mint, and cane sugar (to taste).

--------------------------------------

1. Per preparare questa straordinaria torta, raccogliete i biscotti secchi nel mixer e polverizzateli. Aggiungete il burro fuso e azionate di nuovo l'apparecchio per amalgamare. Utilizzate questo composto per foderare uno stampo rettangolare festonato con fondo amovibile da 35 x 11 cm. Incominciate prima dai bordi e poi terminate con la base livellando con cura con il dorso di un cucchiaio. Ponete il tutto in frigorifero fino al momento del bisogno.

2. Mettete in ammollo la glatina in acqua fredda per il tempo indicato sulla confezione (circa 10 minuti). In una ciotola lavorate a crema, con la planetaria o uno sbattitore elettrico, la ricotta, lo zucchero semolato, 12 foglie di menta tritate finemente, la scorza di 1 lime e il succo filtrato di 3. Lavorate il tutto fino a ottenere un composto omogeneo. In una ciotola a parte montate la panna e incorporatela al composto a base di formaggio in più riprese con movimenti dal basso verso l'alto.

3. Sciogliete la gelatina ben strizzata in una casseruola di piccole dimensioni insieme al rum a fiamma dolce. Unite il tutto al composto preparato e amalgamate, sempre delicatamente, con una spatola.

4. Versate il composto così ottenuto all'interno del guscio di biscotti che nel frattempo avrete preso fuori dal frigorifero. Livellate e fate solidificare in frigo per almeno 3 ore. Poco prima di servire decorate la torta mojito con fette e spicchi di lime, menta fresca e zucchero di canna a piacere.
