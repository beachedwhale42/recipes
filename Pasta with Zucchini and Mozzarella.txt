Pasta with Zucchini and Mozzarella (from ItalianChef.com)

Ingredients:
1 cup olive oil
2 medium zucchini, sliced thinly
flour spread on a plate for dredging
salt & pepper to taste
1 pound of penne pasta
4 cups marinara sauce
1/2 pound fresh mozzarella
1/4 cup pecorino-romano cheese

Notes:
Cook the pasta just short of al dente and then bake the dish only long enough for the cheese to melt and the top to become crispy immediately after assembling it.

Directions:
Heat olive oil in medium saute pan over medium heat. Dredge zucchini in flour, shaking off any excess and fry in batches. Using a slotted spoon transfer them to paper towels, and season with salt & pepper.

Meanwhile, preheat oven to 400°F and bring a pot of liberally salted water to a boil, add pasta and cook until almost al dente, the pasta should be slightly undercooked.

Strain pasta and transfer to a 3 to 4 quart baking dish. Add 2 cups of marinara sauce, zucchini, 1/4 pound of mozzarella and 1/8 cup pecorino-romano, and toss until well combined.

Top with the remaining sauce and sprinkle on the remaining cheeses. Place the baking dish in the oven and bake until the cheese on top is melted and slightly browned, 8-10 minutes.